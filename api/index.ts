import { NowRequest, NowResponse } from "@vercel/node";
import request from "request";
import sharp from "sharp";

export default async function (req: NowRequest, res: NowResponse) {
  const {
    url = "https://cdn2.thecatapi.com/images/u-svD9g4X.jpg",
    width = "640",
    height = "480",
  } = req.query;
  request({ url, encoding: null }, (err, response, body) => {
    if (!err && response.statusCode === 200) {
      sharp(body)
        .resize(parseInt(width), parseInt(height), { fit: "inside" })
        .webp()
        .toBuffer()
        .then((data) => {
          res.setHeader("Content-Type", "image/webp");
          res.setHeader("Cache-Control", "max-age=2592000,public");
          res.send(data);
        })
        .catch((err) => {
          console.log("error", err);
        });
    }
  });
}
